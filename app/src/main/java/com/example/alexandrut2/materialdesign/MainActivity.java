package com.example.alexandrut2.materialdesign;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import static android.support.v7.widget.StaggeredGridLayoutManager.VERTICAL;

public class MainActivity extends AppCompatActivity {


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<String> myDataset = Arrays.asList("Name1", "Name2", "Name3", "Name4", "Name5");
    private static int[] imgs = { R.mipmap.pic1, R.mipmap.pic2, R.mipmap.pic3, R.mipmap.pic4, R.mipmap.pic5 };
    private ImageButton displayModeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new MyAdapter(myDataset, imgs);
        mRecyclerView.setAdapter(mAdapter);
        displayModeButton = (ImageButton) findViewById(R.id.display_mode_button);

        displayModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleMenu();
            }
        });
    }

    private void handleMenu(){

        if(mLayoutManager instanceof  StaggeredGridLayoutManager){
            Log.d("Alex","is Staggered");
            displayModeButton.setImageDrawable(getDrawable(R.drawable.ic_view_grid));
            mLayoutManager = new LinearLayoutManager(this);
        }else if(mLayoutManager instanceof  GridLayoutManager){
            Log.d("Alex","is Grid");
            displayModeButton.setImageDrawable(getDrawable(R.drawable.ic_view_list));
            mLayoutManager = new StaggeredGridLayoutManager(2,VERTICAL);
        } else if(mLayoutManager instanceof LinearLayoutManager){
            Log.d("Alex","is Linear");
            displayModeButton.setImageDrawable(getDrawable(R.drawable.ic_view_staggered));
            mLayoutManager = new GridLayoutManager(this,3);
        }
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }


    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyHolder> {

        private List<String> stringList;
        private int[] imgs;

        class MyHolder extends RecyclerView.ViewHolder {
            TextView text;
            CardView cardView;
            ImageView imgView;
            LinearLayout containerLayout;
            LinearLayout bottomLayout;


            @SuppressLint("ClickableViewAccessibility")
            MyHolder(final View view) {
                super(view);
                text = (TextView) view.findViewById(R.id.info_text);
                cardView = (CardView) view.findViewById(R.id.card_view);
                imgView = (ImageView) view.findViewById(R.id.item_image) ;
                containerLayout = (LinearLayout) view.findViewById(R.id.containerLayout);
                bottomLayout = (LinearLayout) view.findViewById(R.id.bottom_layout);
                // Add animation by code
                /*StateListAnimator animator = AnimatorInflater
                        .loadStateListAnimator(MainActivity.this, R.animator.rise_transition);
                cardView.setStateListAnimator(animator);*/

                //Start next activity
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(MainActivity.this, SecondActivity.class);

                        Bundle b = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, imgView, imgView.getTransitionName()).toBundle();
                        int pos = mLayoutManager.getPosition(view);
                        i.putExtra("img", imgs[pos]);
                        startActivity(i, b);
                    }
                });

                cardView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        TransitionManager.beginDelayedTransition(mRecyclerView);
                        if(bottomLayout.getVisibility() == View.VISIBLE){
                            bottomLayout.setVisibility(View.GONE);
                        }else{
                            bottomLayout.setVisibility(View.VISIBLE);
                        }
                        return true;
                    }
                });


                //code rise Animation

                /*cardView.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                            Utils.animateRise(cardView, true);
                        }
                        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                            Utils.animateRise(cardView, false);
                        }
                        return false;
                    }
                });*/

            }
        }

        MyAdapter(List<String> strings, int[] imgs) {
            this.stringList = strings;
            this.imgs = imgs;
        }




        @Override
        public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_view, parent, false);

            return new MyHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyHolder holder, int position) {
            String s = stringList.get(position);
            holder.text.setText(s);
            holder.imgView.setImageResource(imgs[position]);

            if(mLayoutManager instanceof StaggeredGridLayoutManager || mLayoutManager instanceof GridLayoutManager){
                holder.containerLayout.setOrientation(LinearLayout.VERTICAL);
            } else {
                holder.containerLayout.setOrientation(LinearLayout.HORIZONTAL);
            }


            Utils.setSlideFromLeftAnimation(holder.cardView, MainActivity.this, position);

        }




        @Override
        public int getItemCount() {
            return stringList.size();
        }
    }


}
