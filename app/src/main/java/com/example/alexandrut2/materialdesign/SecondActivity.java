package com.example.alexandrut2.materialdesign;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.graphics.BitmapFactory;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionListenerAdapter;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    View rootLayout;
    ImageView imageView;
    TextView textView;
    View textLayout;
    FloatingActionButton fab;
    ImageView hiddenImage;
    TextView title;
    CollapsingToolbarLayout collapsingToolbarLayout;

    int x = 0,y = 0;

    @SuppressLint({"ClickableViewAccessibility", "NewApi", "RestrictedApi"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_second);
        imageView = (ImageView) findViewById(R.id.image);
        imageView.setImageResource(getIntent().getExtras().getInt("img"));
        hiddenImage = (ImageView) findViewById(R.id.hiddenImage);
        title = (TextView) findViewById(R.id.title);
        ((CollapsingToolbarLayout)findViewById(R.id.collapsing_toolbar)).setTitle("Some Title");
        rootLayout = (View) findViewById(R.id.root_layout);
        textView = (TextView) findViewById(R.id.info_text);
        textLayout = (View) findViewById(R.id.text_layout);
        Utils.setSlideUpAnimation(findViewById(R.id.text_l),this);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        Transition sharedElementEnterTransition = getWindow().getSharedElementEnterTransition();
        sharedElementEnterTransition.addListener(new TransitionListenerAdapter() {
            @Override
            public void onTransitionEnd(Transition transition) {
                fab.show();
            }
        });
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Palette.Builder paletteBuilder = Palette.from(BitmapFactory.decodeResource(getResources(),getIntent().getExtras().getInt("img")));
        Palette palette = paletteBuilder.generate();

        collapsingToolbarLayout.setContentScrimColor(palette.getDominantColor(ContextCompat.getColor(this, R.color.colorPrimary)));
        collapsingToolbarLayout.setStatusBarScrimColor(palette.getDarkMutedColor(R.color.colorPrimaryDark));
        fab.setBackgroundColor(palette.getDarkVibrantColor(ContextCompat.getColor(this, R.color.colorAccent)));

        setActions();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransitionManager.beginDelayedTransition((LinearLayout)findViewById(R.id.text_l));
                if(hiddenImage.getVisibility() == View.VISIBLE){
                    hiddenImage.setVisibility(View.GONE);
                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)title.getLayoutParams();
                    params.gravity = Gravity.START;
                    title.setLayoutParams(params);
                } else {
                    hiddenImage.setVisibility(View.VISIBLE);
                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)title.getLayoutParams();
                    params.gravity = Gravity.CENTER_HORIZONTAL;
                    title.setLayoutParams(params);
                }
            }
        });

        /*Slide slide = Utils.getSlideTransition(this,R.id.text_l);
        getWindow().setEnterTransition(slide);
        slide.addListener(new TransitionListenerAdapter() {
            @Override
            public void onTransitionEnd(Transition transition) {
                getWindow().setEnterTransition(null);
                getWindow().setExitTransition(null);
            }
        });*/
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        fab.setVisibility(View.GONE);
    }

    private void setActions(){
        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                x = (int)motionEvent.getX();
                y = (int)motionEvent.getY();
                return false;
            }
        });

        textLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                x = (int)motionEvent.getX();
                y = (int)motionEvent.getY();
                return false;
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageView.setVisibility(View.GONE);
                float hypot = (float) Math.hypot(imageView.getHeight(), imageView.getWidth());
                Animator a = Utils.createRevealAnimator(textLayout, x,y,0, hypot);
                a.start();
                textLayout.setVisibility(View.VISIBLE);
            }
        });

        textLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textLayout.setVisibility(View.GONE);
                float hypot = (float) Math.hypot(imageView.getHeight(), imageView.getWidth());
                Animator a = Utils.createRevealAnimator(imageView, x,y,0, hypot);
                a.start();
                imageView.setVisibility(View.VISIBLE);
            }
        });
    }
}
