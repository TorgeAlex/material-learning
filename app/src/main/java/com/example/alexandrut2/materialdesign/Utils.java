package com.example.alexandrut2.materialdesign;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.transition.Slide;
import android.transition.Transition;
import android.view.Gravity;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

/**
 * Created by alexandrut2 on 10/23/17.
 */

public class Utils {


    /////// ANIMATIONS
    public static Animator createRevealAnimator(View view, int x, int y, float startRadius, float endRadius){
        Animator animator = ViewAnimationUtils.createCircularReveal(
                view, x, y, startRadius, endRadius
        );
        animator.setDuration(300);
        animator.setInterpolator( new AccelerateDecelerateInterpolator());
        return animator;
    }

    public static void setSlideFromLeftAnimation(View viewToAnimate, Context context, int pos) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_in_from_left);
        //animation.setStartOffset(pos * 100);
        animation.setStartOffset(100);
        viewToAnimate.startAnimation(animation);
    }

    public static void animateRise(View v, boolean isUp){
        int start = isUp ? 0 : 12;
        int end = isUp ? 12 : 0;
        ObjectAnimator animator = ObjectAnimator.ofFloat(v,View.TRANSLATION_Z,start,end);
        animator.setDuration(300);
        animator.start();
    }


    public static void setSlideUpAnimation(View v, Context context){
        Animation slideUpAnimation = AnimationUtils.loadAnimation(context, R.anim.slide_up);
        v.startAnimation(slideUpAnimation);
    }



    public static Slide getSlideTransition(Activity activity, int id){
        Slide transition = new Slide(Gravity.BOTTOM);
        transition.addTarget(id);
        transition.setInterpolator(AnimationUtils.loadInterpolator(activity, android.R.interpolator
                .linear_out_slow_in));
        transition.setDuration(300);
        transition.setStartDelay(300);
        return transition;
    }
}
